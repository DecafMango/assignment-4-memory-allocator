//
// Created by decaf on 13.12.23.
//

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <assert.h>

void debug(const char *fmt, ...);

struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

struct three_blocks {
    struct block_header* block1;
    struct block_header* block2;
    struct block_header* block3;
};

struct three_blocks prepare_heap() {
    void *heap = heap_init(REGION_MIN_SIZE);
    assert(heap);

    void *pointer1 = _malloc(100);
    void *pointer2 = _malloc(100);
    void *pointer3 = _malloc(100);

    struct block_header *block1 = block_get_header(pointer1);
    struct block_header *block2 = block_get_header(pointer2);
    struct block_header *block3 = block_get_header(pointer3);

    assert(!block1->is_free);
    assert(block1->capacity.bytes == 100);
    assert(!block2->is_free);
    assert(block2->capacity.bytes == 100);
    assert(!block3->is_free);
    assert(block3->capacity.bytes == 100);

    return (struct three_blocks) {
        .block1 = block1,
        .block2 = block2,
        .block3 = block3
    };
}

void test_malloc() {
    debug("Test: heap initialization");

    prepare_heap();

    heap_term();
}

void test_single_free() {
    debug("Test: single free");

    struct three_blocks blocks = prepare_heap();

    _free(blocks.block2);

    assert(!blocks.block1->is_free);
    assert(blocks.block2->is_free);
    assert(!blocks.block3->is_free);

    heap_term();
}

void test_multiple_free() {
    debug("Test: multiple free");

    struct three_blocks blocks = prepare_heap();

    _free(blocks.block3);

    assert(!blocks.block1->is_free);
    assert(!blocks.block2->is_free);
    assert(blocks.block3->is_free);

    _free(blocks.block2);

    assert(!blocks.block1->is_free);
    assert(blocks.block2->is_free);
    assert(blocks.block2->capacity.bytes == offsetof(struct block_header, contents) + 100 * 2);

    heap_term();
}

void test_grow_heap() {
    debug("Test: grow heap");
    void *heap = heap_init(REGION_MIN_SIZE);
    assert(heap);

    void *pointer = _malloc(REGION_MIN_SIZE);

    struct block_header *header = block_get_header(pointer);
    assert(header->capacity.bytes == REGION_MIN_SIZE);
    assert(size_from_capacity(header -> capacity).bytes > REGION_MIN_SIZE);

    heap_term();
}

int main() {
    test_malloc();
    test_single_free();
    test_multiple_free();
    test_grow_heap();
    return 0;
}