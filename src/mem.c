#define _DEFAULT_SOURCE

#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {

    // calculating region_size (do you remember that block have a header?)
    size_t region_size = region_actual_size(offsetof(struct block_header, contents) + query);

    // trying to mmap region by given address
    void *region_address = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

    // if region haven't been mapped by given address - try to mmap region somewhere
    if (region_address == MAP_FAILED) {
        region_address = map_pages(addr, region_size, 0);
    }
    // if finally couldn't mmap region - return REGION_INVALID
    if (region_address == MAP_FAILED) {
        return REGION_INVALID;
    }

    // initializing a region structure
    struct region region = {
            .addr = region_address,
            .size = region_size,
            .extends = region_address == addr // it means that region have been mapped by given address
    };

    // fully initializing mmaped region by block
    block_init(region_address, (block_size) {.bytes = region_size}, NULL);

    return region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header* current_header = HEAP_START;

    while (current_header != NULL) {
        size_t current_region_size = 0;
        void* current_region_start = current_header;

        while (current_header -> next == block_after(current_header)) {
            current_region_size += size_from_capacity(current_header -> capacity).bytes;
            current_header = current_header -> next;
        }
        current_region_size += size_from_capacity(current_header -> capacity).bytes;

        current_header = current_header -> next;

        munmap(current_region_start, current_region_size);
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    // query must be greater or equals BLOCK_MIN_CAPACITY
    query = size_max(query, BLOCK_MIN_CAPACITY);

    if (!block_splittable(block, query)) {
        return false;
    }

    // splitting previous block on two blocks
    block_size first_block_size = size_from_capacity((block_capacity) {.bytes = query});
    block_size second_block_size = {size_from_capacity(block->capacity).bytes - first_block_size.bytes};

    // initializing blocks
    void *second_block = (void *) block + first_block_size.bytes;
    block_init(second_block, second_block_size, block->next);
    block_init(block, first_block_size, second_block);

    return true;
}


/*  --- Слияние соседних свободных блоков --- */
static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    struct block_header *next_block = block->next;

    // check next block on NULL and mergeable
    if (next_block == NULL || !mergeable(block, next_block)) {
        return false;
    }

    // if merheable - merge!!!
    block->capacity = (block_capacity) {.bytes = block->capacity.bytes +
                                                 size_from_capacity(next_block->capacity).bytes}; // count new capacity
    block->next = next_block->next; // update next block pointer

    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    while (true) {
        // try to merge current block with next free blocks
        while (try_merge_with_next(block)) {}
        if (block->is_free && block_is_big_enough(sz, block)) {
            return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
        }
        if (block->next == NULL) {
            return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};
        }
        block = block->next;
    }
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result result = find_good_or_last(block, query);

    // if good block wasn't found and reached the end - return
    if (result.type == BSR_REACHED_END_NOT_FOUND) {
        return result;
    }

    // if good block was found - trying to split it
    split_if_too_big(result.block, query);
    result.block->is_free = false;
    return result;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    // checking last block
    if (last == NULL) {
        return NULL;
    }

    // getting address of next region
    void *next_region_address = block_after(last);

    // trying to mmap new region and check it
    struct region next_region = alloc_region(next_region_address, query);
    if (region_is_invalid(&next_region)) {
        return NULL;
    }

    // connecting previous region with next one
    last->next = next_region.addr;

    // if next region extends and last block is free - merge them
    if (next_region.extends && last->is_free) {
        try_merge_with_next(last);
        return last;
    }

    return next_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {

    struct block_search_result result = try_memalloc_existing(query, heap_start);

    // if good block was found - return it
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        return result.block;
    }
    // if good block was not found and heap end was reached - trying to grow heap
    struct block_header *last_block_after_heap_grow = grow_heap(result.block, query);
    // check heap grow result
    if (last_block_after_heap_grow == NULL) {
        return NULL;
    }

    // retry to find good block
    result = try_memalloc_existing(query, heap_start);

    return result.block;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    // merging freed block with next free blocks
    while (try_merge_with_next(header));
}
